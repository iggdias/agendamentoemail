# alura-ejb

Exercícios do curso de EJB da Alura

* Para utilizar o archetype do instrutor, add repo do maven (https://repo1.maven.org/maven2/) com description = Maven Central (validar no botão Verify)
* Wildfly 15 (apontado pelo instrutor na página wildfly.org, apesar de ter utilizado o 14 no eclipse)
* JDK 8
* Connector JDBC MySQL 5.1.15 como dependency no pom.xml
* Para configurar o conector JDBC (arquivos críticos MODULE.XML (wildfly, descrição e driver JDBC), STANDALONE.XML (wildfly, indicação dos data sources), PERSISTENCE.XML (app, indicação da conexões de banco))

> Iniciar o jboss cli no WILDFLY_HOME/bin

> Verificar se existe resquício de configuração em module.xml ou persistence.xml (se houver, remover e reiniciar servidor)

> module add --name=com.mysql --resources=/home/iggdias/.m2/repository/mysql/mysql-connector-java/5.1.15/mysql-connector-java-5.1.15.jar --dependencies=javax.api,javax.transaction.api

> /subsystem=datasources/jdbc-driver=com.mysql:add(driver-name=com.mysql,driver-module-name=com.mysql,driver-xa-datasource-class-name=com.mysql.jdbc.jdbc2.optional.MysqlXADataSource)

> data-source add --name=AgendamentoDS --jndi-name=java:jboss/datasources/AgendamentoDS --driver-name=com.mysql  --connection-url=jdbc:mysql://localhost:3306/agendamentobd --user-name=agendamento --password=agendamento --min-pool-size=10 --max-pool-size=20


* Observações: a configuração do persistence.xml não foi automaticamente gerada (utilizou-se a versão do instrutor)